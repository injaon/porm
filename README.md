pORM (not porn)
====

pORM (or Python ORM) is a simple ORM for mariaDB and MySQL and python3. With a
limited number of features it makes the work done. If you require a more
powerfull ORM take a look at [sqlalchemy](http://www.sqlalchemy.org/). Now,
read the awesome tutorial.

Tutorial
========

Simple by disign.
----------------

```
#!python

import porm

class User(porm.Model):
    pass


porm.connect(host="localhost", db="test", user="root", passwd="root")
usr = User.grab(name="foo",passwd="bar")
usr.name = "new name"
usr.update()

newusr = User()
newusr.name = "Nicolas Cage"
newusr.passwd = "I'm awesome"
newusr.insert()

```

Conventions
-----------

Some conventions are needed in the database.

* Names:  Table name must be written in underscore and the class name in
CamelCase (Pep 8) and they must be coincident. Example:

table name: one_table
class name: OneTable

* Columns: Every table must have an primary key called id.
