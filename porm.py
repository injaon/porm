#!/usr/bin/env python3
# -g*- coding: utf-8 -*-
# @author Gabriel Lopez
import re


CONN = None
DEFAULTDB = None
_models = []


def connect(host=None, user=None, passwd=None, db=None, driver="mysql"):
    """Connect with the database and init the models."""
    global CONN, DEFAULTDB
    if driver == 'sqlite':
        import sqlite3 as dbapi
    elif driver == "mysql":
        import pymysql as dbapi

    DEFAULTDB = db
    CONN = dbapi.connect(host=host, user=user, passwd=passwd, db=db)

    # init models
    cursor = CONN.cursor()
    for model in _models:
        cols = set()
        cursor.execute("DESCRIBE " + model.__tablename__)
        obj = cursor.fetchone()
        while obj:
            cols.add(obj[0])
            obj = cursor.fetchone()
        model.__columns__ = cols
    cursor.close()


def close():
    """Finish the connection."""
    CONN.close()


def _where(where):
    """Process where conditions."""
    wheresttm = ""
    for key, val in where.items():
        if isinstance(val, tuple):
            where[key] = inside(*val)
        elif not isinstance(val, Q):
            where[key] = eq(val)
        wheresttm += "{} {} AND ".format(key, where[key])
    wheresttm += "1"
    return wheresttm


# utilities <<<
def _fmt_query(qry, columns=None, from_=None, where={},
               order_by=None, limit=None, *args):
    """format a select query.
    Where is a dict"""
    if columns is not None and not type(columns) is list:
        raise TypeError("columns must be a list")
    if columns and "id" not in columns:
        columns.append("id")

    fmt = ["*" if columns is None else ",".join(columns)]
    if from_:
        fmt.append(from_)

    fmt.append(_where(where))

    if order_by:
        fmt.append("ORDER BY {} {}".format(
            order_by[0] if isinstance(order_by[0], str) else
            ", ".join(order_by[0]),
            "ASC" if order_by[1] else "DESC"))
    else:
        fmt.append("")

    if limit:
        fmt.append("LIMIT {}".format(limit if isinstance(limit, int) else
                                     ",".join([str(val) for val in limit])))
    else:
        fmt.append("")

    if args:
        fmt.append(*args)
    return qry.format(*fmt)


def _fmt_model(obj, row, cursor):
    desc = cursor.description
    for i in range(len(row)):
        setattr(obj, desc[i][0], row[i])  # desc[i][0] => column name
    return obj


def _convert(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def flatten(seq):
    result = []
    for el in seq:
        if hasattr(el, "__iter__") and not isinstance(el, str):
            result.extend(flatten(el))
        else:
            result.append(el)
    return result
# >>> utilities


class ModelMetaClass(type):
    """set __tablename__ in every model."""
    def __new__(cls, name, base, attributes):
        clazz = type.__new__(cls, name, base, attributes)

        if clazz.__name__ != "Model":
            clazz.__tablename__ = _convert(clazz.__name__)
            if getattr(clazz, "__schema__", None):
                clazz.__tablename__ = "{}.{}".format(
                    clazz.__schema__, clazz.__tablename__)
            _models.append(clazz)

        return clazz


class Model(object, metaclass=ModelMetaClass):
    """Most important class. Every model must inherit from this class."""
    __columns__ = None

    def __init__(self, **kwargs):
        for key, val in kwargs.items():
            setattr(self, key, val)

    @classmethod
    def delete(cls, **kwargs):
        sttm = "DELETE FROM {} WHERE {}".format(
            cls.__tablename__,
            _where(kwargs))
        cursor = CONN.cursor()
        cursor.execute(sttm, flatten([val.obj for val in kwargs.values()]))
        CONN.commit()
        cursor.close()

    @classmethod
    def grab(cls, **kwargs):
        """Returns one model."""
        rs = cls.find(_limit_=1, **kwargs)
        return None if len(rs) == 0 else rs.pop()

    @classmethod
    def find(cls, **kwargs):
        """find <cls> objects"""
        result = []
        cursor = CONN.cursor()
        _order_by = None
        if kwargs.get("_order_by_", None) or \
           kwargs.get("_order_by_asc_", None):
            _order_by = (
                kwargs.pop("_order_by_", None) or kwargs.pop("_order_by_asc_"),
                True)
        elif kwargs.get("_order_by_desc_", None):
            _order_by = (
                kwargs.pop("_order_by_desc_"),
                False)

        cursor.execute(_fmt_query(
            "SELECT {} FROM {} WHERE {} {} {}",
            kwargs.pop("_columns_", None),
            cls.__tablename__,
            limit=kwargs.pop("_limit_", None),
            where=kwargs,
            order_by=_order_by),
            flatten([val.obj for val in kwargs.values()]))

        obj = cursor.fetchone()
        while obj:
            result.append(_fmt_model(cls(), obj, cursor))
            obj = cursor.fetchone()

        cursor.close()
        return result

    def insert(self):
        """Add this object to the database"""
        if hasattr(self, "id"):
            return TypeError("Object already inserted.")

        keys = list(self.__dict__.keys())
        sttm = "INSERT INTO {} ({}) values ({})".format(
            type(self).__tablename__, ",".join(keys),
            ",".join(["%s"] * len(self.__dict__)))

        cursor = CONN.cursor()
        cursor.execute(sttm, [getattr(self, key) for key in keys])
        CONN.commit()
        self.id = cursor.lastrowid
        cursor.close()
        for attr in type(self).__columns__:
            if hasattr(self, attr):
                continue
            setattr(self, attr, None)
        return self

    def remove(self):
        """delete this object from database"""
        cursor = CONN.cursor()
        sttm = "DELETE FROM {} WHERE id={}".format(
            type(self).__tablename__, self.id)
        cursor.execute(sttm)
        CONN.commit()
        cursor.close()

    def update(self, _columns_=None):
        """update this obj from the database"""
        cursor = CONN.cursor()
        keys = _columns_ or list(self.__dict__.keys())
        if "id" in keys:
            keys.pop(keys.index("id"))
        sttm = "UPDATE {} SET {} WHERE id={}".format(
            type(self).__tablename__,
            ",".join(["{} = %s".format(key)
                      for key in keys]),
            self.id)

        cursor.execute(sttm, [self.__dict__[key] for key in keys])
        CONN.commit()
        cursor.close()

    def __repr__(self):
        return "{} {}".format(type(self), str(self.__dict__))

    def __eq__(self, other):
        return type(self) is type(other) and self.__dict__ == other.__dict__


# Q for complex queries. Idea taken from django's ORM.
class Q(object):
    def __init__(self, clause=None, obj=None):
        self.clause = clause
        self.obj = obj

    def __str__(self):
        return self.clause


# some useful operators.
def lt(other):
    return Q("< %s", other)


def lte(other):
    return Q("<= %s", other)


def eq(other):
    return Q("= %s", other)


def nq(other):
    return Q("<> %s", other)


def gt(other):
    return Q("> %s", other)


def gte(other):
    return Q(">= %s", other)


def inside(*other):
    return Q("IN ({})".format(",".join(["%s"] * len(other))), other)


def contains(other):
    return Q("LIKE %s", "%" + other + "%")


def startswith(other):
    return Q("LIKE %s", other + "%")


def endswith(other):
    return Q("LIKE %s", "%" + other)
