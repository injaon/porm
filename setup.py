#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @author Gabriel Lopez


from distutils.core import setup


setup(name="pORM",
      version="0.01-dev",
      url="https://bitbucket.org/injaon/porm/overview",
      license="BSD",
      platforms="any",
      description="Simple Python ORM for MySQL and mariaDB",
      author="Gabriel López",
      author_email="gabriel.marcos.lopez@gmail.com",
      py_modules=["porm"]
)
