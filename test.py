#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @author Gabriel Lopez
import porm
import pytest
import pymysql


class User(porm.Model):
    pass


def setup_module():
    conn = pymysql.connect(
        host="localhost", user="root", passwd="root", db="foo")
    cursor = conn.cursor()
    cursor.execute("""create table user (
    id serial primary key,
    name varchar(20),
    passwd varchar(20)
    )""")
    conn.commit()
    cursor.close()
    conn.close()
    porm.connect(host="localhost", user="root", passwd="root", db="foo")


def teardown_module():
    conn = pymysql.connect(
        host="localhost", user="root", passwd="root", db="foo")
    cursor = conn.cursor()
    cursor.execute("drop table user")
    conn.commit()
    cursor.close()
    conn.close()


class TestModel(object):
    def teardown(self):
        User.delete()

    def test_operations(self):
        # insert
        user = User()
        user.name = "Nicolas Cage"
        user.passwd = "I'm cool, bro"
        user.insert()

        # find & grab
        result = User.grab(id=user.id)
        assert result is not user
        assert result == user
        assert User.find(id=user.id) == [user]
        assert User.find() == [user]

        # update
        user.passwd = user.passwd.replace("cool", "very cool")
        user.update()
        result = User.grab(id=user.id)
        assert result is not user
        assert result == user
        assert User.find(id=user.id) == [user]
        # update specified colums
        user.name = "injaon"
        user.update(_columns_=["passwd"])
        assert user != User.grab(id=user.id)
        user.name = "Nicolas Cage"
        assert user == User.grab(id=user.id)

        # remove
        user.remove()
        assert User.grab(id=user.id) is None
        assert User.find(id=user.id) == []

    def test_init(self):
        user = User(name="foo", passwd="bar")
        assert user.name == "foo"
        assert user.passwd == "bar"

    def test_tablename(self):
        class Foo(porm.Model):
            pass
        assert Foo.__tablename__ == "foo"

        class Fooo(porm.Model):
            __schema__ = "foobar"
        assert Fooo.__tablename__ == "foobar.fooo"

        class FooBar(porm.Model):
            __schema__ = "foobar"
        assert FooBar.__tablename__ == "foobar.foo_bar"

    def test_order_by(self):
        one = User(name="foo", passwd="bar").insert()
        two = User(name="bar", passwd="foo").insert()

        rs = User.find(_order_by_="name")
        assert rs == [two, one]
        rs = User.find(_order_by_asc_="name")
        assert rs == [two, one]
        rs = User.find(_order_by_desc_="name")
        assert rs == [one, two]

        # order by list...
        three = User(name="bar", passwd="taz").insert()
        rs = User.find(_order_by_=["name", "id"])
        assert rs == [two, three, one]

    def test_limit(self):
        one = User(name="foo", passwd="bar").insert()
        two = User(name="bar", passwd="foo").insert()

        rs = User.find(id=one.id, _limit_=1)
        assert rs == [one]
        rs = User.find(_order_by_="id", _limit_=(1, 1))
        assert rs == [two]

    def test_find(self):
        one = User(name="foo", passwd="bar").insert()
        two = User(name="bar", passwd="foo").insert()

        # numeric conditions
        assert User.find(id=porm.gt(one.id)) == [two]
        assert User.find(id=porm.gte(one.id)) == [one, two]
        assert User.find(id=porm.eq(one.id)) == [one]
        assert User.find(id=porm.nq(one.id)) == [two]
        assert User.find(id=porm.lt(one.id)) == []
        assert User.find(id=porm.lte(one.id)) == [one]
        # value in list
        assert User.find(id=porm.inside(one.id, two.id, 3)) == [one, two]
        # str conditions
        assert User.find(name=porm.contains("oo")) == [one]
        assert User.find(name=porm.startswith("f")) == [one]
        assert User.find(passwd=porm.endswith("ar")) == [one]

    def test_delete(self):
        one = User(name="foo", passwd="bar").insert()
        two = User(name="bar", passwd="foo").insert()
        User.delete(id=porm.gte(one.id))
        assert User.find() == []

    def test_model_init(self):
        one = User().insert()
        assert User.__columns__ == set(["id", "name", "passwd"])
        assert one.id > 0
        assert one.name is None
        assert one.passwd is None
